<?php
try {
    $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)); // tester la connexion à la bdd
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage()); // si ratée affichée la connexion à la bdd
}
// Si tout va bien, on peut continuer


// **************************** recupere les messages

if (isset($_POST['inputname']) AND isset($_POST['textarea'])) {
    setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
    $names = htmlspecialchars($_POST['inputname']);
    $messages = htmlspecialchars($_POST['textarea']);
    $temps =  strftime("%A %d %B %Y à %Hh%M "); //Affichera par exemple "date du jour en français : samedi 24 juin 2006." ;


    $rqt_recup = $bdd->prepare('INSERT INTO minichat(name , message, temp ) VALUES(:names , :messages , :temp )');

    $rqt_recup->execute(array(
        'names' => $names,
        'messages' => $messages,
        'temp' => $temps ));

    $rqt_recup->closeCursor(); // Termine le traitement de la requête


}
?>

<?php

// **************************** affiche les messages

$rqt_aff = $bdd->query('SELECT name , message, temp FROM minichat ORDER BY id '); // On récupère tout le contenu de la table minichat non traitable en l'état


while ($posts = $rqt_aff->fetch()) // On affiche ligne par ligne chaque entrée une à une tant que $reponse->fetch = true

{
    echo "<p classes='chatline'><strong classes='names'>" . $posts['name'] . "</strong> -  " . $posts['message'] . "
        <br><span classes='temps' >Posté le  ".$posts['temp']."</span></p>";
}

$rqt_aff->closeCursor(); // Termine le traitement de la requête

?>






