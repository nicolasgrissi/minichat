<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mini chat</title>
    <link rel="stylesheet" href="app.css">
</head>
<body>

<div class="wrapper">

    <h1 class="title-heading">Mini-chat</h1>
    <div class="input">

        <form action="minichat.php" method="post" class="input-form">


            <label for="inputname" class="input-label">Nom : </label> <br><input type="text" name="inputname" id=""
                    class="input-field" placeholder="ex : Jean" required>
            <br>
            <br>
            <label for="textarea" class="input-label">Message : </label><br><textarea name="textarea" id="" cols="30"
                    rows="1" class="input-field" placeholder="Votre message ici" required></textarea>
            <br>
            <br>

            <button type="submit">Envoyer  	&rarr;</button>

        </form>

    </div>
    <div class="message">

    <h2 class="title-messages">Tous les messages</h2> <a href="minichat.php" class="refresh-btn">Rafraichir les messages</a>
    <?php
     require "minichat_post.php";
    ?>
    </div>

</div>


</body>
</html>




